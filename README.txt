------------------------------------------------------------------------------------------------------
Configure Nodes:
------------------------------------------------------------------------------------------------------
python -m da configure_nodes/configure_nodes.da config/basic-algo.properties

------------------------------------------------------------------------------------------------------
Run application:
------------------------------------------------------------------------------------------------------
python -m da -n Main -f –-logfilename logs/basic-algo.log -L info src/Master.da config/basic-algo.properties

------------------------------------------------------------------------------------------------------
Test Cases
------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------
MAIN FILES
--------------------------------------------------------------------------------------------------------
1. Master.da - It contains the Master code which is used for running the complete setup.
2. Client.da - It contains the client code which takes care of sending the request.
3. Coordinator.da - It contains the code for the Coordinator which is responsible for handling subject and resource operations.
4. Database.da - It contains the code for database which is responsible for storing data during evaluation.
5. Worker.da - It has the code which takes care of policy evaluation for the request.
6. system.properties & variants - It contains properties being used to bring up the complete setup and data required for running the system.
7. policy.xml - It contains the policy data in the form of xml which is used by the worker for policy evaluation.
8. db-data.xml - It contains the data required for loading initial db.

--------------------------------------------------------------------------------------------------------
BUGS AND LIMITATIONS
--------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------
CONTRIBUTIONS
--------------------------------------------------------------------------------------------------------
The contribution stated below is just an approximate division of work between the team members.
Most part of the code has been worked upon by both the team members and the below segregation should not be treated for clear work boundaries.

