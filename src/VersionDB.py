class Version:
    object = None
    attribute = None
    attribute_value = None
    read_timestamp = None
    write_timestamp = None
    pendingMightRead = []
    pendingMightWrite = []

    # def __init__(self):
    #     print("This is default constructor")

    # def __init__(self, object, attribute, attribute_value, read_timestamp,
    #              write_timestamp, pendingMightRead, pendingMightWrite):
    # self.object = object
    # self.attribute = attribute
    # self.attribute_value = attribute_value
    # self.read_timestamp = read_timestamp
    # self.write_timestamp = write_timestamp
    # self.pendingMightRead = pendingMightRead
    # self.pendingMightWrite = pendingMightWrite

def update_latest_Version(data_versions, object, attributes, read_timestamp, write_timestamp):

    print("Attributes: " + str(attributes))
    i = 0
    for attribute in attributes:
        if i % 2 == 0:
            ver = Version()
            ver.object = object
            ver.attribute = attribute
            #ver.value = attributes[i+1]
            ver.write_timestamp = write_timestamp
            ver.read_timestamp = read_timestamp
            ver.pendingMightRead = []
            ver.pendingMightWrite = []
            data_versions.append(ver)
            print("Len of data_version: " + str(len(data_versions)))
            i += 1

    return data_versions

def latestVersionBefore(data_versions, object, attribute, timestamp):
    list_version = []
    for version in data_versions:
        if version.object == object and version.attribute == attribute:
            list_version.append(version)

    if list_version == []:
        ver = Version()
        ver.object = object
        ver.attribute = attribute
        ver.read_timestamp = 0
        ver.write_timestamp = 0
        #data_versions.append(ver)
        return ver

    # Iterate through the list of version and return the first version with timestamp less than timestamp in the function parameter.
    for version in reversed(list_version):
        if version.write_timestamp < timestamp:
            return version


def print_version(version):
    print(version.object)
    print(version.attribute)
    print(version.attribute_value)
    print(version.read_timestamp)
    print(version.write_timestamp)
    print(version.pendingMightRead)
    print(version.pendingMightWrite)